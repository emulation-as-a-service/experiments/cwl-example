# cwl-example

Exemplary CWL Worfklow that can be used with the rewriter API:

https://historic-builds.emulation.cloud/emil/workflow/api/v1/rewrite

Example Request (POST):

```json
{
    "rewriteURL" : "https://github.com/Aeolic/example-workflow/blob/main/example_workflow.cwl"
}
```
